<?php

use App\Categories;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Categories::create([
			'name' => 'Strzelanka'
		]);

		Categories::create([
			'name' => 'Strategia'
		]);

		Categories::create([
			'name' => 'Bijatyka'
		]);

		Categories::create([
			'name' => 'Platformowa'
		]);

		Categories::create([
			'name' => 'Przygodowa'
		]);

		Categories::create([
			'name' => 'RPG'
		]);

		Categories::create([
			'name' => 'MMORPG'
		]);

		Categories::create([
			'name' => 'Wyścigowa'
		]);

		Categories::create([
			'name' => 'Ekonomiczna'
		]);

		Categories::create([
			'name' => 'Tower defense'
		]);
    }
}
