<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert([
			'name' => 'Tomasz',
			'email' => 'tomasz.brudzinski@gmail.com',
			'password' => bcrypt('geniek123'),
		]);

		DB::table('users')->insert([
			'name' => 'Kazimierz',
			'email' => 'test@test.pl',
			'password' => bcrypt('test123'),
		]);
    }
}
