<?php

use App\Platforms;
use Illuminate\Database\Seeder;

class PlatformsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Platforms::create([
			'name' => 'Windows'
		]);

		Platforms::create([
			'name' => 'Mac OS X'
		]);

		Platforms::create([
			'name' => 'Linux'
		]);
    }
}
