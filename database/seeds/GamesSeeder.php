<?php

use App\Games;
use Illuminate\Database\Seeder;

class GamesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Games::create([
			'category_id' => 3,
			'platform_id' => 1,
        	'name' => 'Nazwa gry 1',
			'description' => 'opis wciągającej gry 1',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 1',
			'publisher' => 'Wydawca gry 1'
		]);

		Games::create([
			'category_id' => 4,
			'platform_id' => 2,
			'name' => 'Nazwa gry 2',
			'description' => 'opis wciągającej gry 2',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 2',
			'publisher' => 'Wydawca gry 2'
		]);

		Games::create([
			'category_id' => 5,
			'platform_id' => 3,
			'name' => 'Nazwa gry 3',
			'description' => 'opis wciągającej gry 3',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 3',
			'publisher' => 'Wydawca gry 3'
		]);

		Games::create([
			'category_id' => 6,
			'platform_id' => 1,
			'name' => 'Nazwa gry 4',
			'description' => 'opis wciągającej gry 4',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 4',
			'publisher' => 'Wydawca gry 4'
		]);

		Games::create([
			'category_id' => 7,
			'platform_id' => 2,
			'name' => 'Nazwa gry 5',
			'description' => 'opis wciągającej gry 5',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 5',
			'publisher' => 'Wydawca gry 5'
		]);

		Games::create([
			'category_id' => 8,
			'platform_id' => 3,
			'name' => 'Nazwa gry 6',
			'description' => 'opis wciągającej gry 6',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 6',
			'publisher' => 'Wydawca gry 6'
		]);

		Games::create([
			'category_id' => 9,
			'platform_id' => 1,
			'name' => 'Nazwa gry 7',
			'description' => 'opis wciągającej gry 7',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 7',
			'publisher' => 'Wydawca gry 7'
		]);

		Games::create([
			'category_id' => 10,
			'platform_id' => 2,
			'name' => 'Nazwa gry 8',
			'description' => 'opis wciągającej gry 8',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 8',
			'publisher' => 'Wydawca gry 8'
		]);

		Games::create([
			'category_id' => 1,
			'platform_id' => 3,
			'name' => 'Nazwa gry 9',
			'description' => 'opis wciągającej gry 9',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 9',
			'publisher' => 'Wydawca gry 9'
		]);

		Games::create([
			'category_id' => 2,
			'platform_id' => 1,
			'name' => 'Nazwa gry 10',
			'description' => 'opis wciągającej gry 10',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 10',
			'publisher' => 'Wydawca gry 10'
		]);

		Games::create([
			'category_id' => 3,
			'platform_id' => 2,
			'name' => 'Nazwa gry 11',
			'description' => 'opis wciągającej gry 11',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 11',
			'publisher' => 'Wydawca gry 11'
		]);

		Games::create([
			'category_id' => 4,
			'platform_id' => 3,
			'name' => 'Nazwa gry 12',
			'description' => 'opis wciągającej gry 12',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 12',
			'publisher' => 'Wydawca gry 12'
		]);

		Games::create([
			'category_id' => 5,
			'platform_id' => 1,
			'name' => 'Nazwa gry 13',
			'description' => 'opis wciągającej gry 13',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 13',
			'publisher' => 'Wydawca gry 13'
		]);

		Games::create([
			'category_id' => 6,
			'platform_id' => 2,
			'name' => 'Nazwa gry 14',
			'description' => 'opis wciągającej gry 14',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 14',
			'publisher' => 'Wydawca gry 14'
		]);

		Games::create([
			'category_id' => 7,
			'platform_id' => 3,
			'name' => 'Nazwa gry 15',
			'description' => 'opis wciągającej gry 15',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 15',
			'publisher' => 'Wydawca gry 15'
		]);

		Games::create([
			'category_id' => 8,
			'platform_id' => 1,
			'name' => 'Nazwa gry 16',
			'description' => 'opis wciągającej gry 16',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 16',
			'publisher' => 'Wydawca gry 16'
		]);

		Games::create([
			'category_id' => 9,
			'platform_id' => 2,
			'name' => 'Nazwa gry 17',
			'description' => 'opis wciągającej gry 17',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 17',
			'publisher' => 'Wydawca gry 17'
		]);

		Games::create([
			'category_id' => 10,
			'platform_id' => 0,
			'name' => 'Nazwa gry 18',
			'description' => 'opis wciągającej gry 18',
			'release_date' => '2017-11-20',
			'rate' => 7.2,
			'engine' => 'Unity',
			'producer' => 'Producent gry 19',
			'publisher' => 'Wydawca gry 20'
		]);
    }
}
