<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Games extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('category_id');
			$table->tinyInteger('platform_id');
            $table->char('name',255);
			$table->string('description')->nullable();
            $table->date('release_date')->nullable();
			$table->decimal('rate', 2, 1)->nullable();
			$table->char('engine',255)->nullable();
			$table->char('producer',255)->nullable();
			$table->char('publisher',255)->nullable();
			$table->char('photo',25)->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
