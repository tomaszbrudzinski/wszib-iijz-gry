#1. Temat zadania

Należy stworzyć aplikację, umożliwiającą użytkownikom przeglądanie bazy opisów gier/gry i dodawanie nowych, poprzez odpowiedni formularz. Należy także zaimplementować formularz zakładania sobie kont uczestników oraz logowania. Dla bezpieczeństwa, każde logowanie powinno być wpisywane do bazy wraz z IP.
Należy użyć HTML/CSS oraz PHP z bazą SQLite. Można wykorzystać częściowo stronę z lekcji 4 i formularz z 5.

#2. Jak uruchomić gotowy projekt ?

Pliki projektu zawierają pakiety zainstalowane przez Composer'a (katalog `vendor`), oraz plik bazy danych SQLITE (plik `database/database.sqlite`), stąd nie jest konieczne uruchamianie composera, oraz inicjalizacja i migracje bazy danych.

Projekt mozna uruchomić:
 
- poprzez serwer Apache, otwierając w przeglądarce lokalizację katalogu `public`
 
 lub uruchamiając wbudowany w PHP serwer na porcie 8000 w następujący sposób:

- `php artisan serve` lub `php -S localhost:8000 -t public`

**UWAGA: Projekt wymaga PHP w wersji >=7.0**

#3. Uruchomienie czystego projektu bez danych od podstaw

W swoim dostępnym publicznie repozytorium [Gitlab.com](https://gitlab.com/tomaszbrudzinski/wszib-iijz-gry) przechowuję projekt bez katalogów "vendor" oraz bez bazy danych sqlite. Gdyby zaszła potrzeba uruchomienia takiego czystego projektu, należy wykonać kolejno:

- `git clone https://gitlab.com/tomaszbrudzinski/wszib-iijz-gry.git .`
- `composer install`
- `cp .env.example .env ` (moja przykładowa konfiguracja używa SQLITE)
- `touch database/database.sqlite`
- `php artisan migrate:install`
- `php artisan migrate`
- `php artisan key:generate`


#4. Dalszy development 

Projekt oprócz frameworku PHP Laravel wykorzystuje także Bootstrap oraz SASS jako preprocesor CSS.

Aby pracować z skryptami JS oraz SASS należy zainstalować moduły NPM:

- `npm install`

A następnie by skompilować źródłowe pliki JS/SASS znajdujące się w `resourcers/assets` do ich wyjściowych postaci w katalogu `public/`:

- `npm run dev

`