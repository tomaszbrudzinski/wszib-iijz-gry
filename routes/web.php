<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'GamesController@list')->name('games');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('games')->group(function () {

	Route::get('search', 'GamesController@search')->name('search');
	Route::get('details/{id}', 'GamesController@details')->name('details');
	Route::match(['get','post'],'edit/{id}', 'GamesController@edit')->name('edit');
	Route::match(['get','post'],'add', 'GamesController@add')->name('add');
	Route::delete('delete/{id}', 'GamesController@delete')->name('delete');
});
