<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Games;
use App\Categories;
use App\Platforms;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Route;

class GamesController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth', ['only' => ['new','edit', 'delete']]);
	}

	private $games;
	private $game;

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function list($page = 1)
	{
		$games = Games::query()->simplePaginate(10);
		$categories = Categories::all();

		return view('games.list',['games' => $games, 'categories' => $categories, 'type' => 'list']);
	}

	public function search(Request $request, $page = 1) {

		$name = $request->input('name');
		$category_id = $request->input('category_id');
		$games = Games::query();
		if($name){
			$games = $games->where('name','LIKE','%'.$name.'%');
		}
		if($category_id){
			$games = $games->where('category_id','LIKE','%'.$category_id.'%');
		}
		$games = $games->get()->forPage($page,20)->all();
		$categories = Categories::all();

		return view('games.list',['games' => $games, 'categories' => $categories, 'type' => 'search']);
	}

	public function details($game_id)
	{
		$game = Games::where('id',$game_id)->first();
		$platform = Platforms::where('id',$game->platform_id)->first();
		$category = Categories::where('id',$game->category_id)->first();
		$game->platform_name = $platform->name;
		$game->category_name = $category->name;

		return view('games.details',['game' => $game]);
	}

	public function edit($game_id, Request $request)
	{

		$game = Games::where('id',$game_id)->first();

		if($request->method()=='POST') {
			$this->saveGameData($game, $request);
			return redirect('/');
		}

		$categories = Categories::all();
		$platforms = Platforms::all();

		return view('games.edit',['game' => $game, 'categories' => $categories, 'platforms' => $platforms, 'type' => 'edit']);
	}

	public function add(Request $request)
	{

		$game = new Games;
		if($request->method()=='POST') {
			$this->saveGameData($game, $request);
			return redirect('/');
		}

		$categories = Categories::all();
		$platforms = Platforms::all();

		return view('games.edit',['game' => $game, 'categories' => $categories, 'platforms' => $platforms, 'type' => 'add']);
	}

	public function delete($game_id)
	{
		$game = Games::where('id',$game_id)->first();
		$game->delete();

		return redirect('/');
	}

	private function saveGameData($game, Request $request){

		if ($request->file('photo')) {
			if($request->file('photo')->isValid()) {
				$file = $request->file('photo');
				$filename = time() . '.' . $file->getClientOriginalExtension();
				$request->file('photo')->move("photos", $filename);
				$game->photo = $filename;
			}
		}

		$game->name = $request->input('name');
		$game->category_id = $request->input('category_id');
		$game->platform_id = $request->input('platform_id');
		$game->description = $request->input('description');
		$game->release_date = $request->input('release_date');
		$game->rate = $request->input('rate');
		$game->engine = $request->input('engine');
		$game->producer = $request->input('producer');
		$game->publisher = $request->input('publisher');
		$game->save();
	}

}
