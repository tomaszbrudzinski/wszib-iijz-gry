<?php

namespace App\Listeners;

use App\Events\Event;
use Illuminate\Auth\Events\Login;
use App\Logs;

class LoginEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
	 *
	 * Save user login details to logs table
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(Login $login)
    {
		$logs = new Logs;
		$logs->time = $_SERVER['REQUEST_TIME'];
		$logs->user_id = $login->user->id;
		$logs->ip_address = $_SERVER['REMOTE_ADDR'];
		$logs->save();
    }
}
