<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logs extends Model
{
	// Table does not contain "created_at" and "updated_at" columns
	public $timestamps = false;
}
