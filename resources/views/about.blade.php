@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">O projekcie</div>

					<div class="panel-body">
						<h3>1. Zadanie</h3>
						<p>Należy stworzyć aplikację, umożliwiającą użytkownikom przeglądanie bazy opisów gier/gry i dodawanie nowych, poprzez odpowiedni formularz. Należy także zaimplementować formularz zakładania sobie kont uczestników oraz logowania. Dla bezpieczeństwa, każde logowanie powinno być wpisywane do bazy wraz z IP.
							Należy użyć HTML/CSS oraz PHP z bazą SQLite. Można wykorzystać częściowo stronę z lekcji 4 i formularz z 5.
						</p>

						<h3>2. Informacje dodatkowe</h3>
						<p>Do realizacji projektu użyłem frameworku PHP Laravel 5.5 oraz Bootstrap 3 (SASS). </p>
						<p>Repozytorium projektu znajduje się pod adresem: <a href="https://gitlab.com/tomaszbrudzinski/wszib-iijz-gry" target="_blank">https://gitlab.com/tomaszbrudzinski/wszib-iijz-gry</a></p>

						<h3>3. Autor</h3>
						<p>Tomasz Brudziński, gr. 2k331</p>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
