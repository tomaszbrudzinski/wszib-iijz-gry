@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Witaj {{ $user->name }}</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    Zostałeś poprawnie zalogowany. Możesz teraz edytować <a href="{{ route('games') }}">listę gier</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
