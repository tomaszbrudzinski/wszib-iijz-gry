@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if(count($games)>0)
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <td><strong>#</strong></td>
                                    <td></td>
                                    <td><strong>Nazwa</strong></td>
                                    <td class="hidden-xs"><strong>Wydawca</strong></td>
                                    <td class="hidden-xs"><strong>Data wydania</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                        @foreach ($games as $key=>$game)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>
                                    @if($game->photo)
                                        <img src="{{ url('/photos') }}/{{ $game->photo }}" alt="" style="max-width: 50px; max-height: 50px;" />
                                    @endif
                                </td>
                                <td><a href="{{ route('details',$game->id) }}">{{ $game->name  }}</a></td>
                                <td class="hidden-xs hidden-sm">{{ $game->publisher  }}</td>
                                <td class="hidden-xs">{{ $game->release_date }}</td>
                                <td>
                                    <a href="{{ route('details',$game->id) }}" class="btn btn-info">Szczegóły</a>
                                    @if (Auth::check())
                                        <a href="{{ route('edit',$game->id) }}" class="btn btn-warning hidden-xs">Edytuj</a>
                                        <form method="POST" action="{{ route('delete',$game->id) }}" style="display: inline-block">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <div class="form-group">
                                                <input type="submit" class="btn btn-danger delete hidden-xs" value="Usuń">
                                            </div>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                            </tbody>
                        </table>
                        @if($type=='list')
                            <div class="text-center">
                                {{ $games->render() }}
                            </div>
                        @endif
                    @else
                        <p>Nie znaleziono gier spełniających podane kryteria</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Wyszukiwarka</div>

                <div class="panel-body">
                    <form method="GET" action="{{ route('search') }}">
                        <div class="form-group">
                            <label for="name">Nazwa gry</label>
                            <input type="text" name="name" class="form-control" placeholder="nazwa...">
                        </div>
                        <div class="form-group">
                            <label for="category_id">Kategoria</label>
                            <select name="category_id" class="form-control">
                                <option value="">Wybierz kategorię:</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="text-center">
                            <a href="{{ route('games') }}" class="btn btn-info">Wyczyść filtr</a>
                            <input type="submit" class="btn btn-warning" value="filtruj" />
                        </div>

                    </form>
                </div>
            </div>
            @if (Auth::check())
                <div class="panel panel-default">
                    <div class="panel-heading">Redagowanie</div>

                    <div class="panel-body text-center">
                        <a href="{{ route('add') }}" class="btn btn-success">Dodaj nową grę</a>
                    </div>
                </div>
            @endif;
        </div>
    </div>
</div>
@endsection
