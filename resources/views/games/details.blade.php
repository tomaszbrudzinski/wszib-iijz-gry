

@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Szczegóły - {{ $game->name }}</div>

                    <div class="panel-body">

                        <div class="text-center">
                        @if($game->photo)
                            <img src="{{ url('/photos') }}/{{ $game->photo }}" alt="" style="max-width: 50%;" />
                        @endif
                        <h3 style="">{{ $game->name }}</h3>
                        </div>
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>Kategoria:</td>
                                    <td>{{ $game->category_name}}</td>
                                </tr>
                                <tr>
                                    <td>Platforma:</td>
                                    <td>{{ $game->platform_name}}</td>
                                </tr>
                                <tr>
                                    <td>Opis:</td>
                                    <td>{{ $game->description}}</td>
                                </tr>
                                <tr>
                                    <td>Data wydania:</td>
                                    <td>{{ $game->release_date}}</td>
                                </tr>
                                <tr>
                                    <td>Ocena:</td>
                                    <td>{{ $game->rate}}</td>
                                </tr>
                                <tr>
                                    <td>Silnik:</td>
                                    <td>{{ $game->engine}}</td>
                                </tr>
                                <tr>
                                    <td>Producent:</td>
                                    <td>{{ $game->producer}}</td>
                                </tr>
                                <tr>
                                    <td>Wydawca:</td>
                                    <td>{{ $game->publisher}}</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="text-center">
                            @if (Auth::check())
                                <a href="{{ route('games') }}" class="btn btn-info">Powrót</a>
                                <a href="{{ route('edit',$game->id) }}" class="btn btn-warning">Edytuj</a>
                            <br /><br />
                                <form method="POST" action="{{ route('delete',$game->id) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <div class="form-group">
                                        <input type="submit" class="btn btn-danger delete" value="Usuń grę">
                                    </div>
                                </form>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
