

@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $type=='edit' ? 'Edycja' : 'Dodawanie nowej gry' }}</div>

                    <div class="panel-body">

                        <form method="POST" action="{{ route($type,$game->id) }}" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="name">Nazwa</label>
                                <input name="name" value="{{ $game->name }}" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="category_id">Kategoria</label>
                                <select name="category_id" class="form-control">
                                    <option value="">Wybierz kategorię:</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ $category->id == $game->category_id ? 'selected' : ''}}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="platform_id">Platforma</label>
                                <select name="platform_id" class="form-control">
                                    <option value="">Platforma:</option>
                                    @foreach($platforms as $platform)
                                        <option value="{{ $platform->id }}" {{ $platform->id == $game->platform_id ? 'selected' : ''}}>{{ $platform->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                @if($game->photo)
                                    <img src="{{ url('/photos') }}/{{ $game->photo }}" alt="" style="max-width: 50px; max-height: 50px;" />
                                @endif
                                <label for="photo">Zdjęcie</label>
                                <input type="file" name="photo" accept="image/*">
                            </div>
                            <div class="form-group">
                                <label for="description">Opis</label>
                                <textarea name="description" class="form-control">{{ $game->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="release_date">Data</label>
                                <input name="release_date" type="date" value="{{ $game->release_date }}" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="rate">Ocena</label>
                                <input name="rate" value="{{ $game->rate }}" class="form-control"></input>
                            </div>
                            <div class="form-group">
                                <label for="engine">Silnik</label>
                                <input name="engine" value="{{ $game->engine }}" class="form-control"></input>
                            </div>
                            <div class="form-group">
                                <label for="producer">Producent</label>
                                <input name="producer" value="{{ $game->producer }}" class="form-control"></input>
                            </div>
                            <div class="form-group">
                                <label for="publisher">Wydawca</label>
                                <input name="publisher" value="{{ $game->publisher }}" class="form-control"></input>
                            </div>

                            <!-- CSRF token is necessary for POST form -->
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="text-center">
                                <a href="{{ route('games') }}" class="btn btn-info">Anuluj</a>
                                <input type="submit" class="btn btn-warning" value="Zapisz" />
                            </div>
                        </form>
                        <br />
                        <div class="text-center">
                            <form method="POST" action="{{ route('delete',$game->id) }}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <div class="form-group">
                                    <input type="submit" class="btn btn-danger delete" value="Usuń" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
